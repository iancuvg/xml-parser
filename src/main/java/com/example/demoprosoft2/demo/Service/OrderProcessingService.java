package com.example.demoprosoft2.demo.Service;

import com.example.demoprosoft2.demo.Input.Orders;
import com.example.demoprosoft2.demo.Input.Product;
import com.example.demoprosoft2.demo.Output.ProductOutput;
import com.example.demoprosoft2.demo.Output.ProductsOutput;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import java.util.stream.Collectors;

@Service
public class OrderProcessingService {

    private static final Logger log = LoggerFactory.getLogger(OrderProcessingService.class);

    @Value("${input.folder.path}")
    private String inputFolderPath;

    @Value("${output.folder.path}")
    private String outputFolderPath;

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() throws IOException, XMLStreamException {
        File inputFolder = new File(inputFolderPath);
        if (inputFolder.isDirectory() && inputFolder.list().length > 0) {
            log.info("Files found in folder " + inputFolderPath);
            for (File file: inputFolder.listFiles()) {
                log.info("Processing " + file.getName());
                InputStream xmlResource = new FileInputStream(file);
                XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
                XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(xmlResource);


                XmlMapper xmlMapper = new XmlMapper();
                Orders orders = xmlMapper.readValue(xmlStreamReader, Orders.class);
                Map<String, ProductsOutput> output = orders.getOrders().stream()
                        .flatMap(order -> order.getProducts().stream().map(product -> mapToProductOutput(product, order.getID(), order.getCreated())))
                        .sorted(Comparator.comparing(ProductOutput::getOrderDate).reversed())
                        .sorted(Comparator.comparing(ProductOutput::getPrice).reversed())
                        .collect(Collectors.groupingBy(ProductOutput::getSupplier,
                                Collectors.collectingAndThen(Collectors.toList(), ProductsOutput::new)))
                        ;

                for (String fileName : output.keySet()) {
                    xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
                    String supplierFileName = outputFolderPath + File.separator + fileName + ".xml";
                    log.info("Writing supplier file " + supplierFileName);
                    xmlMapper.writeValue(new File(supplierFileName), output.get(fileName));
                }

                file.delete();
            }
        } else {
            log.info("No file found!");
        }

    }

    private ProductOutput mapToProductOutput(Product product, String orderID, Date orderDate) {
        return ProductOutput.builder()
                .description(product.getDescription())
                .gtin(product.getGtin())
                .price(product.getPrice())
                .orderID(orderID)
                .orderDate(orderDate)
                .supplier(product.getSupplier().concat(orderID.substring(0,2)))
                .build();
    }

}
