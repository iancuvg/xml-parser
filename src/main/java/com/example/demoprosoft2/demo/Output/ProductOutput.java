package com.example.demoprosoft2.demo.Output;

import com.example.demoprosoft2.demo.Input.Price;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@Builder
public class ProductOutput {

    private String description;
    private String gtin;
    private Price price;
    private String orderID;
    @JsonIgnore
    private Date orderDate;
    @JsonIgnore
    private String supplier;

}
