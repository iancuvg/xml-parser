package com.example.demoprosoft2.demo.Input;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Order {

    @JacksonXmlProperty(localName = "product")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Product> products;

    @JacksonXmlProperty(isAttribute = true)
    private Date created;

    @JacksonXmlProperty(isAttribute = true)
    private String ID;

}
