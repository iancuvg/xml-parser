package com.example.demoprosoft2.demo.Input;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Price implements Comparable<Price>{

    @JacksonXmlProperty(isAttribute = true)
    private String currency;

    @JacksonXmlText
    private BigDecimal valueOfPrice;

    @Override
    public int compareTo(Price p) {
        if (getValueOfPrice() == null || p.getValueOfPrice() == null) {
            return 0;
        }
        return getValueOfPrice().compareTo(p.getValueOfPrice());
    }

}
