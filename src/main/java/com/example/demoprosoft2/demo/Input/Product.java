package com.example.demoprosoft2.demo.Input;

import lombok.Data;

@Data
public class Product {

    private String description;
    private String gtin;
    private Price price;
    private String supplier;
    private String orderId;

}
