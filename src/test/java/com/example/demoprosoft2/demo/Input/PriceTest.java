package com.example.demoprosoft2.demo.Input;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;



class PriceTest {

    @Test
    void testGetCurrency() {
        //given
        String currency = "USD";
        Price price = new Price();

        //when
        price.setCurrency(currency);

        //then
        assertThat(currency).isEqualTo(price.getCurrency());
    }

    @Test
    void testGetValueOfPrice() {
        //given
        BigDecimal value = new BigDecimal("10.00");
        Price price = new Price();

        //when
        price.setValueOfPrice(value);

        //then
        assertThat(value).isEqualTo(price.getValueOfPrice());
    }

    @Test
    void testCompareTo() {
        //given
        BigDecimal value1 = new BigDecimal("10.00");
        BigDecimal value2 = new BigDecimal("5.00");
        BigDecimal value3 = new BigDecimal("15.00");
        BigDecimal value4 = null;
        Price price1 = new Price();
        Price price2 = new Price();
        Price price3 = new Price();
        Price price4 = new Price();

        //when
        price1.setValueOfPrice(value1);
        price2.setValueOfPrice(value2);
        price3.setValueOfPrice(value3);
        price4.setValueOfPrice(value4);

        //then
        assertThat(0).isEqualTo(price1.compareTo(price1));
        assertThat(1).isEqualTo(price1.compareTo(price2));
        assertThat(-1).isEqualTo(price1.compareTo(price3));
        assertThat(0).isEqualTo(price1.compareTo(price4));
    }
}