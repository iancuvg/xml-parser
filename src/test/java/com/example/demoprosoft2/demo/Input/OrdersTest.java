package com.example.demoprosoft2.demo.Input;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;


import static org.junit.jupiter.api.Assertions.*;

class OrdersTest {

    @Test
    void testGetOrders() {

        //given
        Order order1 = new Order();
        Order order2 = new Order();
        List<Order> orderList = Arrays.asList(order1, order2);
        Orders orders = new Orders();

        //when
        orders.setOrders(orderList);

        //then
        assertThat(orderList).isEqualTo(orders.getOrders());
    }
}