package com.example.demoprosoft2.demo.Input;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

class OrderTest {
    @Test
    void testGetProducts() {
        //given
        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> productList = Arrays.asList(product1, product2);
        Order order = new Order();

        //when
        order.setProducts(productList);

        //then
        assertThat(productList).isEqualTo(order.getProducts());
    }

    @Test
    void testGetCreated() {
        //given
        Date date = new Date();
        Order order = new Order();

        //when
        order.setCreated(date);

        //then
        assertThat(date).isEqualTo(order.getCreated());
    }

    @Test
    void testGetID() {
        //given
        String ID = "123";
        Order order = new Order();

        //when
        order.setID(ID);

        //then
        assertThat(ID).isEqualTo(order.getID());
    }
}