package com.example.demoprosoft2.demo.Input;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @Test
    void testGetDescription() {
        //given
        String description = "A description";
        Product product = new Product();

        //when
        product.setDescription(description);

        //then
        assertThat(description).isEqualTo(product.getDescription());
    }

    @Test
    void testGetGtin() {
        //given
        String gtin = "A gtin";
        Product product = new Product();

        //when
        product.setGtin(gtin);

        //then
        assertThat(gtin).isEqualTo(product.getGtin());
    }

    @Test
    void testGetPrice() {
        //given
        Price price = new Price();
        Product product = new Product();

        //when
        product.setPrice(price);

        //then
        assertThat(price).isEqualTo(product.getPrice());
    }

    @Test
    void testGetSupplier() {
        //given
        String supplier = "A supplier";
        Product product = new Product();

        //when
        product.setSupplier(supplier);

        //then
        assertThat(supplier).isEqualTo(product.getSupplier());
    }

    @Test
    void testGetOrderId() {
        //given
        String orderId = "An orderId";
        Product product = new Product();

        //when
        product.setOrderId(orderId);

        //then
        assertThat(orderId).isEqualTo(product.getOrderId());
    }


}