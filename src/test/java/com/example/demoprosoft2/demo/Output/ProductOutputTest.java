package com.example.demoprosoft2.demo.Output;

import com.example.demoprosoft2.demo.Input.Price;
import com.example.demoprosoft2.demo.Input.Product;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProductOutputTest {


    @Test
    void testTheWholeProductOutput() {
        //given
        String description = "A description";
        String gtin = "A gtin";
        Price price = new Price();
        String orderID = "An order id";
        Date orderDate = new Date();
        String supplier = "A supplier";

        //when
        ProductOutput productOutput = new ProductOutput(description, gtin, price, orderID, orderDate, supplier);

        //then
        assertThat(description).isEqualTo(productOutput.getDescription());
        assertThat(gtin).isEqualTo(productOutput.getGtin());
        assertThat(price).isEqualTo(productOutput.getPrice());
        assertThat(orderID).isEqualTo(productOutput.getOrderID());
        assertThat(orderDate).isEqualTo(productOutput.getOrderDate());
        assertThat(supplier).isEqualTo(productOutput.getSupplier());
    }

    @Test
    void testGetTheWholeProduct() {
        //given
        String description = "A description";
        String gtin = "A gtin";
        Price price = new Price();
        String orderID = "An order id";
        Date orderDate = new Date();
        String supplier = "A supplier";
        ProductOutput productOutput = new ProductOutput(description, gtin, price, orderID, orderDate, supplier);

        //when
        productOutput.setDescription(description);
        productOutput.setGtin(gtin);
        productOutput.setPrice(price);
        productOutput.setOrderID(orderID);
        productOutput.setOrderDate(orderDate);
        productOutput.setSupplier(supplier);

        //then
        assertThat(description).isEqualTo(productOutput.getDescription());
        assertThat(gtin).isEqualTo(productOutput.getGtin());
        assertThat(price).isEqualTo(productOutput.getPrice());
        assertThat(orderID).isEqualTo(productOutput.getOrderID());
        assertThat(orderDate).isEqualTo(productOutput.getOrderDate());
        assertThat(supplier).isEqualTo(productOutput.getSupplier());
    }

    @Test
    void testBuilder() {

        //given
        String description = "A description";
        String gtin = "A gtin";
        Price price = new Price();
        String orderID = "An order id";
        Date orderDate = new Date();
        String supplier = "A supplier";

        //when
        ProductOutput productOutput = ProductOutput.builder()
                .description(description)
                .gtin(gtin)
                .price(price)
                .orderID(orderID)
                .orderDate(orderDate)
                .supplier(supplier)
                .build();

        //then
        assertThat(description).isEqualTo(productOutput.getDescription());
        assertThat(gtin).isEqualTo(productOutput.getGtin());
        assertThat(price).isEqualTo(productOutput.getPrice());
        assertThat(orderID).isEqualTo(productOutput.getOrderID());
        assertThat(orderDate).isEqualTo(productOutput.getOrderDate());
        assertThat(supplier).isEqualTo(productOutput.getSupplier());
    }

}