package com.example.demoprosoft2.demo.Output;

import com.example.demoprosoft2.demo.Input.Order;
import com.example.demoprosoft2.demo.Input.Orders;
import com.example.demoprosoft2.demo.Input.Price;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ProductsOutputTest {

    @Test
    void testGetProducts() {

        //given
        String description1 = "A description 1";
        String gtin1 = "A gtin 1";
        Price price1 = new Price();
        String orderID1 = "An order id 1";
        Date orderDate1 = new Date();
        String supplier1 = "A supplier 1";

        String description2 = "A description 2";
        String gtin2 = "A gtin 2";
        Price price2 = new Price();
        String orderID2 = "An order id 2";
        Date orderDate2 = new Date();
        String supplier2 = "A supplier 2";

        ProductOutput productOutput1 = new ProductOutput(description1, gtin1, price1, orderID1, orderDate1, supplier1);
        ProductOutput productOutput2 = new ProductOutput(description2, gtin2, price2, orderID2, orderDate2, supplier2);
        List<ProductOutput> productOutputList = Arrays.asList(productOutput1, productOutput2);
        ProductsOutput productsOutput = new ProductsOutput(productOutputList);

        //when
        productsOutput.setProducts(productOutputList);

        //then
        assertThat(productOutputList).isEqualTo(productsOutput.getProducts());
    }

}