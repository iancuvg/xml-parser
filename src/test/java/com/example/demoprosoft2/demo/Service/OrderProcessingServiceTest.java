package com.example.demoprosoft2.demo.Service;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ReflectionUtils;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderProcessingServiceTest {

    @Spy
    private OrderProcessingService orderProcessingService;

    @Mock
    private Logger log;

    @Mock
    private File inputFolder;


    @Before
    public void setUp() {
        orderProcessingService = new OrderProcessingService();
        ReflectionTestUtils.setField(orderProcessingService, "inputFolderPath", "/home/misk/Desktop/XML/input");
        ReflectionTestUtils.setField(orderProcessingService, "outputFolderPath", "/home/misk/Desktop/XML/output");
    }

    @Test
    @SneakyThrows
    public void testReportCurrentTime_withInputFiles() {

        when(inputFolder.isDirectory()).thenReturn(true);
        when(inputFolder.list()).thenReturn(new String[]{"orders23.xml"});

        orderProcessingService.reportCurrentTime();

        verify(log, times(0)).info("No file found!");

    }

    @Test
    @SneakyThrows
    public void testReportCurrentTime_withoutInputFiles() {
        when(inputFolder.isDirectory()).thenReturn(true);
        when(inputFolder.list()).thenReturn(new String[]{});

        orderProcessingService.reportCurrentTime();

        verify(log, times(1)).info("No file found!");
    }
}