#README

###Input & Output
Pentru inceput, am nevoie de obiecte, atat pentru input -> pentru a reusi sa citesc fisierele XML folosind 
aceasta aplicatie, cat si pentru output -> pentru a reusi sa scriu fisierele XML citite intr-un format anume.

Pentru input avem urmatoarele clase :

-> Orders : Root Element -> lista de obiecte de tip order;

-> Order : Element + 2 Attributes -> lista de obiecte de tip product;

-> Product : Element -> contine 4 elemente pentru a putea descrie produsul;

-> Price : Element -> unul din cele 4 elemente din product, contine un atribut;
aparte "currency" ce va fi considerat ca un String.

Pentru output avem urmatoarele clase :

-> ProductsOutput : Root Element -> lista de obiecte de tip ProductOutput;

-> ProductOutput : Element -> contine 4 elemente pentru a putea descrie
produsul prelucrat.

###Service

Datele sunt prelucrate cu ajutorul clasei OrderProcessingService.

Initial, a trebuit sa preiau datele xml din fisier, lucru pe care l-am facut cu ajutorul unui 
XMLStreamReader, care primea initial fisierul sample cu exemplul dat in cerinta.

Citirea fisierului am memorat-o in obiectul orders.

Mai departe, a trebuit sa "despic firul in 4", deoarece obiectul orders e o lista de liste
de produse. Asadar, am folosit un stream pentru a ajunge la obiectele de tip product.
Problema era ca obiectele de tip product din input trebuiau sa fie putin diferite fata de
cele de output (in input gaseai supplier field, iar in output trebuia sa fie orderid), asa ca 
am creat un builder pentru a-mi face obiectul de output asa cum il voiam. 
Pentru a putea tine evidenta obiectelor cu supplieri diferiti, am folosit groupingBy pentru supplier.
Astfel, am memorat in variabila output de tip map, cheile de tip String pentru supplier, si obiectele de tip
ProductOutput aferente supplier-ilor.
Apoi, pentru fiecare element din output, am creat un fisier cu numele fiecarui supplier, fisier pe care, 
l-am populat cu obiectele de tip ProductOutput, formatate intr-o versiune xml.

@Scheduled e o adnotare pe care am folosit-o in scopul mentinerii aplicatiei, atunci cand este pornita,
intr-o verificare continua odata la 5000 de milisecunde. Odata ce fisierul care urmeaza sa fie prelucrat
intra in directorul input, aplicatia incepe prelucrarea de date. Odata ce s-a terminat aceasta prelucrare,
fisierul din input va fi sters.

###Extra feature

De asemenea, am pus cateva log-uri prin cod pentru a fi putin mai prietenos in consola atunci cand ruleaza.

-> THE END! <-